#define _USE_MATH_DEFINES
#include <stdio.h>              
#include <math.h>

void main()
{
    printf("Enter 'a': \n");
    float a;
    scanf_s("%f", &a);
    printf("a = %f\n\n", a);

    printf("Enter 'x': \n");
    float x;
    scanf_s("%f", &x);
    printf("x = %f\n\n", x);

    float G = (10 * (-45 * pow(a, 2) + 49 * a * x + 6 * pow(x, 2))) / (15 * pow(a, 2) + 49 * a * x + 24 * pow(x, 2));
    float F = tan((5 * pow(a, 2) + 34 * a * x + 45 * pow(x, 2)) * M_PI / 180);
    float Y = -1 * asin((7 * pow(a, 2) - a * x - 8 * pow(x, 2)) * M_PI / 180);

    printf("G = %f\n\n", G);
    printf("F = %f\n\n", F);
    printf("Y = %f\n\n", Y);
}
